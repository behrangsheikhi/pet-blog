<?php
require_once '../function/helper.php';
require_once '../function/connection.php';

global $connect;

?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>بلاگ | جزئیات</title>
    <!--css files-->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700|Playfair+Display:400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="<?= asset('asset/font/icomoon/style.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/magnific-popup.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/jquery-ui.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/owl.carousel.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/owl.theme.default.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/bootstrap-datepicker.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/fonts/flaticon/font/flaticon.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/aos.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
</head>

<body style="overflow-x: hidden;">

    <div class="container-fluid">
        <?php require_once '../layout/header.php'; ?>
    </div>

    <div class="site-wrap">

        <div class="site-mobile-menu">
            <div class="site-mobile-menu-header">
                <div class="site-mobile-menu-close mt-3">
                    <span class="icon-close2 js-menu-toggle"></span>
                </div>
            </div>
            <div class="site-mobile-menu-body"></div>
        </div>

        <header class="site-navbar" role="banner">
            <div class="container-fluid">
                <div class="row align-items-center">

                    <div class="col-12 search-form-wrap js-search-form">
                        <form method="get" action="#">
                            <input type="text" id="s" class="form-control" placeholder="Search...">
                            <button class="search-btn" type="submit"><span class="icon-search"></span></button>
                        </form>
                    </div>
                </div>
        </header>

        <?php
        global $connect;
        $query = 'SELECT pet_blog_db.posts_tbl.* ,pet_blog_db.categories_tbl.name AS category_name FROM posts_tbl JOIN categories_tbl ON posts_tbl.category_id = categories_tbl.id WHERE posts_tbl.status = 1 AND posts_tbl.id = ? ';
        $statement = $connect->prepare($query);
        $statement->execute([$_GET['post_id']]);
        $post = $statement->fetch();
        if ($post !== false) {
        ?>
            <div class="site-cover site-cover-sm same-height overlay single-page">
                <div class="container">
                    <img src="<?= asset($post->image); ?>" alt="post image" style="width : 100%;height : 100vh;margin : 0 auto;">
                    <div class="row same-height justify-content-center">
                        <div class="col-md-12 col-lg-10">
                            <div class="post-entry text-center">
                                <!-- <span class=""></span> -->
                                <a href="<?= url('app/category.php?cat_id=').$post->category_id; ?>" class="post-category text-white bg-success mb-3"><?= $post->category_name; ?></a>
                                <h1 class="mb-4"><a href="#"><?= $post->title; ?></a></h1>
                                <div class="post-meta align-items-center text-center">
                                    <!-- <figure class="author-figure mb-0 mr-3 d-inline-block"><img src="images/person_1.jpg" alt="Image" class="img-fluid"></figure> -->
                                    <!-- <span class="d-inline-block mt-1">By Carrol Atkinson</span> -->
                                    <span>&nbsp;&nbsp; <?= $post->created_at; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

        <section class="site-section py-lg">
            <div class="container">

                <div class="row blog-entries element-animate">

                    <div class="col-lg-12 col-lg-8 main-content">

                        <div class="post-content-body text-right">
                            <p><?= $post->body; ?></p>


                        </div>
                        <p class="text-right"> : دسته بندی های مرتبط</p>
                        <?php
                        $query = 'SELECT * FROM pet_blog_db.categories_tbl';
                        $statement = $connect->prepare($query);
                        $statement->execute();
                        $categories = $statement->fetchAll();
                        foreach ($categories as $category) {
                        ?>
                            <span class="pt-5 text-right float-right">
                                <a href="#"> <span class="text-muted"> | </span> <?= $category->name ?> </a>
                            </span>

                        <?php } ?>


                        <!-- <div class="pt-5">
                            <h3 class="mb-5">6 Comments</h3>
                            <ul class="comment-list">
                                <li class="comment">
                                    <div class="vcard">
                                        <img src="images/person_1.jpg" alt="Image placeholder">
                                    </div>
                                    <div class="comment-body">
                                        <h3>Jean Doe</h3>
                                        <div class="meta">January 9, 2018 at 2:21pm</div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                                        <p><a href="#" class="reply rounded">Reply</a></p>
                                    </div>
                                </li>

                                <li class="comment">
                                    <div class="vcard">
                                        <img src="images/person_1.jpg" alt="Image placeholder">
                                    </div>
                                    <div class="comment-body">
                                        <h3>Jean Doe</h3>
                                        <div class="meta">January 9, 2018 at 2:21pm</div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                                        <p><a href="#" class="reply rounded">Reply</a></p>
                                    </div>

                                    <ul class="children">
                                        <li class="comment">
                                            <div class="vcard">
                                                <img src="images/person_1.jpg" alt="Image placeholder">
                                            </div>
                                            <div class="comment-body">
                                                <h3>Jean Doe</h3>
                                                <div class="meta">January 9, 2018 at 2:21pm</div>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                                                <p><a href="#" class="reply rounded">Reply</a></p>
                                            </div>


                                            <ul class="children">
                                                <li class="comment">
                                                    <div class="vcard">
                                                        <img src="images/person_1.jpg" alt="Image placeholder">
                                                    </div>
                                                    <div class="comment-body">
                                                        <h3>Jean Doe</h3>
                                                        <div class="meta">January 9, 2018 at 2:21pm</div>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                                                        <p><a href="#" class="reply rounded">Reply</a></p>
                                                    </div>

                                                    <ul class="children">
                                                        <li class="comment">
                                                            <div class="vcard">
                                                                <img src="images/person_1.jpg" alt="Image placeholder">
                                                            </div>
                                                            <div class="comment-body">
                                                                <h3>Jean Doe</h3>
                                                                <div class="meta">January 9, 2018 at 2:21pm</div>
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                                                                <p><a href="#" class="reply rounded">Reply</a></p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li class="comment">
                                    <div class="vcard">
                                        <img src="images/person_1.jpg" alt="Image placeholder">
                                    </div>
                                    <div class="comment-body">
                                        <h3>Jean Doe</h3>
                                        <div class="meta">January 9, 2018 at 2:21pm</div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                                        <p><a href="#" class="reply rounded">Reply</a></p>
                                    </div>
                                </li>
                            </ul>
                            

                            <div class="comment-form-wrap pt-5">
                                <h3 class="mb-5">Leave a comment</h3>
                                <form action="#" class="p-5 bg-light">
                                    <div class="form-group">
                                        <label for="name">Name *</label>
                                        <input type="text" class="form-control" id="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email *</label>
                                        <input type="email" class="form-control" id="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="website">Website</label>
                                        <input type="url" class="form-control" id="website">
                                    </div>

                                    <div class="form-group">
                                        <label for="message">Message</label>
                                        <textarea name="" id="message" cols="30" rows="10" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" value="Post Comment" class="btn btn-primary">
                                    </div>

                                </form>
                            </div>
                        </div> 

                    </div>

                 END main-content -->

                        <div class="col-md-12 col-lg-4 sidebar">
                            <div class="sidebar-box search-form-wrap">
                                <form action="#" class="search-form">
                                    <div class="form-group">
                                        <span class="icon fa fa-search"></span>
                                        <input type="text" class="form-control" id="s" placeholder="Type a keyword and hit enter">
                                    </div>
                                </form>
                            </div>
                            <!-- END sidebar-box -->
                            <!-- <div class="sidebar-box">
                            <div class="bio text-center">
                                <img src="images/person_2.jpg" alt="Image Placeholder" class="img-fluid mb-5">
                                <div class="bio-body">
                                    <h2>Craig David</h2>
                                    <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem facilis sunt repellendus excepturi beatae porro debitis voluptate nulla quo veniam fuga sit molestias minus.</p>
                                    <p><a href="#" class="btn btn-primary btn-sm rounded px-4 py-2">Read my bio</a></p>
                                    <p class="social">
                                        <a href="#" class="p-2"><span class="fa fa-facebook"></span></a>
                                        <a href="#" class="p-2"><span class="fa fa-twitter"></span></a>
                                        <a href="#" class="p-2"><span class="fa fa-instagram"></span></a>
                                        <a href="#" class="p-2"><span class="fa fa-youtube-play"></span></a>
                                    </p>
                                </div>
                            </div>
                        </div> -->
                            <!-- END sidebar-box -->
                            <!-- <div class="sidebar-box">
                            <h3 class="heading">Popular Posts</h3>
                            <div class="post-entry-sidebar">
                                <ul>
                                    <li>
                                        <a href="">
                                            <img src="images/img_1.jpg" alt="Image placeholder" class="mr-4">
                                            <div class="text">
                                                <h4>There’s a Cool New Way for Men to Wear Socks and Sandals</h4>
                                                <div class="post-meta">
                                                    <span class="mr-2">March 15, 2018 </span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <img src="images/img_2.jpg" alt="Image placeholder" class="mr-4">
                                            <div class="text">
                                                <h4>There’s a Cool New Way for Men to Wear Socks and Sandals</h4>
                                                <div class="post-meta">
                                                    <span class="mr-2">March 15, 2018 </span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <img src="images/img_3.jpg" alt="Image placeholder" class="mr-4">
                                            <div class="text">
                                                <h4>There’s a Cool New Way for Men to Wear Socks and Sandals</h4>
                                                <div class="post-meta">
                                                    <span class="mr-2">March 15, 2018 </span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div> -->
                            <!-- END sidebar-box -->

                            <!-- <div class="sidebar-box">
                            <h3 class="heading">Categories</h3>
                            <ul class="categories">
                                <li><a href="#">Food <span>(12)</span></a></li>
                                <li><a href="#">Travel <span>(22)</span></a></li>
                                <li><a href="#">Lifestyle <span>(37)</span></a></li>
                                <li><a href="#">Business <span>(42)</span></a></li>
                                <li><a href="#">Adventure <span>(14)</span></a></li>
                            </ul>
                        </div> -->
                            <!-- END sidebar-box -->

                            <!-- <div class="sidebar-box">
                            <h3 class="heading">Tags</h3>
                            <ul class="tags">
                                <li><a href="#">Travel</a></li>
                                <li><a href="#">Adventure</a></li>
                                <li><a href="#">Food</a></li>
                                <li><a href="#">Lifestyle</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">Freelancing</a></li>
                                <li><a href="#">Travel</a></li>
                                <li><a href="#">Adventure</a></li>
                                <li><a href="#">Food</a></li>
                                <li><a href="#">Lifestyle</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">Freelancing</a></li>
                            </ul>
                        </div> -->
                        </div>
                        <!-- END sidebar -->

                    </div>
                </div>
        </section>

        <div class="site-section bg-light">
            <div class="container">

                <div class="row mb-5 text-right">
                    <div class="col-12 font-weight-bold">
                        <h2>پست های مرتبط</h2>
                    </div>
                </div>

                <div class="row align-items-stretch retro-layout">

                    <div class="col-md-5 order-md-2">
                        <a href="single.html" class="hentry img-1 h-100 gradient" style="background-image: url('images/img_4.jpg');">
                            <span class="post-category text-white bg-danger">Travel</span>
                            <div class="text">
                                <h2>The 20 Biggest Fintech Companies In America 2019</h2>
                                <span>February 12, 2019</span>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-7">

                        <a href="single.html" class="hentry img-2 v-height mb30 gradient" style="background-image: url('images/img_1.jpg');">
                            <span class="post-category text-white bg-success">Nature</span>
                            <div class="text text-sm">
                                <h2>The 20 Biggest Fintech Companies In America 2019</h2>
                                <span>February 12, 2019</span>
                            </div>
                        </a>

                        <div class="two-col d-block d-md-flex">
                            <a href="single.html" class="hentry v-height img-2 gradient" style="background-image: url('images/img_2.jpg');">
                                <span class="post-category text-white bg-primary">Sports</span>
                                <div class="text text-sm">
                                    <h2>The 20 Biggest Fintech Companies In America 2019</h2>
                                    <span>February 12, 2019</span>
                                </div>
                            </a>
                            <a href="single.html" class="hentry v-height img-2 ml-auto gradient" style="background-image: url('images/img_3.jpg');">
                                <span class="post-category text-white bg-warning">Lifestyle</span>
                                <div class="text text-sm">
                                    <h2>The 20 Biggest Fintech Companies In America 2019</h2>
                                    <span>February 12, 2019</span>
                                </div>
                            </a>
                        </div>

                    </div>
                </div>

            </div>
        </div>


        <div class="site-section bg-lightx">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-md-5">
                        <div class="subscribe-1 ">
                            <h2>در خبرنامه عضو شوید</h2>
                            <p class="mb-5">جهت دریافت جدیدترین اخبار، در خبرنامه ما عضو شوید</p>
                            <form action="#" class="d-flex">
                                <input type="text" class="form-control text-right" placeholder="ایمیل خود را وارد کنید ">
                                <input type="submit" class="btn btn-primary" value="عضویت">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>






    <?php require_once '../layout/footer.php';  ?>

    <!--js files-->
    <script src="<?= asset('asset/js/jquery-3.3.1.min.js') ?>"></script>
    <script src="<?= asset('asset/js/jquery-migrate-3.0.1.min.js') ?>"></script>
    <script src="<?= asset('asset/js/jquery-ui.js') ?>"></script>
    <script src="<?= asset('asset/js/popper.min.js') ?>"></script>
    <script src="<?= asset('asset/js/bootstrap.min.js') ?>"></script>
    <script src="<?= asset('asset/js/owl.carousel.min.js') ?>"></script>
    <script src="<?= asset('asset/js/jquery.stellar.min.js') ?>"></script>
    <script src="<?= asset('asset/js/jquery.countdown.min.js') ?>"></script>
    <script src="<?= asset('asset/js/jquery.magnific-popup.min.js') ?>"></script>
    <script src="<?= asset('asset/js/bootstrap-datepicker.min.js') ?>"></script>
    <script src="<?= asset('asset/js/aos.js') ?>"></script>

    <script src="<?= asset('asset/js/main.js') ?>"></script>
</body>

</html>
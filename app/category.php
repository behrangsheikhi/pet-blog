<?php
require_once '../function/helper.php';
require_once '../function/connection.php';
require_once '../function/reload.php';

global $connect;

$not_found = false;

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>بلاگ | دسته بندی</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700|Playfair+Display:400,700,900" rel="stylesheet">

  <link rel="stylesheet" href="<?= asset('asset/fonts/icomoon/style.css') ?>">
  <link rel="stylesheet" href="<?= asset('asset/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?= asset('asset/css/magnific-popup.css') ?>">
  <link rel="stylesheet" href="<?= asset('asset/css/jquery-ui.css') ?>">
  <link rel="stylesheet" href="<?= asset('asset/css/owl.carousel.min.css') ?>">
  <link rel="stylesheet" href="<?= asset('asset/css/owl.theme.default.min.css') ?>">
  <link rel="stylesheet" href="<?= asset('asset/css/bootstrap-datepicker.css') ?>">
  <link rel="stylesheet" href="<?= asset('asset/fonts/flaticon/font/flaticon.css') ?>">
  <link rel="stylesheet" href="<?= asset('asset/css/aos.css') ?>">

  <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
</head>

<body>

  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <?php require_once '../layout/header.php';  ?>


    <?php
    if (isset($_GET['cat_id']) and $_GET['cat_id'] !== '') {
      $query = 'SELECT * FROM pet_blog_db.categories_tbl WHERE id = ?';
      $statement = $connect->prepare($query);
      $statement->execute([$_GET['cat_id']]);
      $category = $statement->fetch();
      if ($category !== false) {
    ?>
        <div class="py-5 bg-light">
          <div class="container">
            <div class="row  d-flex justify-content-end">
              <div class="col-md-6 text-right">
                <h3><?= $category->name; ?></h3>
                <p><?= $category->about; ?></p>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
      <div class="site-section bg-white">
        <div class="container">
          <div class="row">

            <?php
            $query = 'SELECT * FROM pet_blog_db.posts_tbl WHERE status = 1 AND category_id = ?';
            $statement = $connect->prepare($query);
            $statement->execute([$_GET['cat_id']]);
            $posts = $statement->fetchAll();
            foreach ($posts as $post) {
            ?>
              <div class="col-lg-4 mb-4">
                <div class="entry2">
                  <a href="single.html"><img src="<?= asset($post->image) ?>" alt="Image" class="img-fluid rounded"></a>
                  <div class="excerpt">
                    <span class="post-category text-white bg-secondary mb-3 text-right"><?= $post->title; ?></span>

                    <h2 class="text-right"><a class="text-right" href="single.html"><?= $category->about; ?></a></h2>
                    <div class="post-meta align-items-center text-left clearfix">
                      <!-- <figure class="author-figure mb-0 mr-3 float-left"><img src="images/person_1.jpg" alt="Image" class="img-fluid"></figure> -->
                      <!-- <span class="d-inline-block mt-1">By <a href="#">Carrol Atkinson</a></span> -->
                      <span>&nbsp;&nbsp; <?= $post->created_at; ?></span>
                    </div>

                    <p class="text-right"><?= substr($post->body,0,200); ?></p>
                    <p class="text-right"><a class="btn btn-primary" href="<?= url('app/detail.php?post_id=').$post->id; ?>">ادامه مطلب</a></p>
                  </div>
                </div>
              </div>
          <?php
            }
          }
          ?>








          <!-- <div class="col-lg-4 mb-4">
            <div class="entry2">
              <a href="single.html"><img src="images/img_2.jpg" alt="Image" class="img-fluid rounded"></a>
              <div class="excerpt">
                <span class="post-category text-white bg-success mb-3">Nature</span>

                <h2><a href="single.html">The AI magically removes moving objects from videos.</a></h2>
                <div class="post-meta align-items-center text-left clearfix">
                  <figure class="author-figure mb-0 mr-3 float-left"><img src="images/person_1.jpg" alt="Image" class="img-fluid"></figure>
                  <span class="d-inline-block mt-1">By <a href="#">Carrol Atkinson</a></span>
                  <span>&nbsp;-&nbsp; July 19, 2019</span>
                </div>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium sed optio, explicabo ad deleniti impedit facilis fugit recusandae! Illo, aliquid, dicta beatae quia porro id est.</p>
                <p><a href="#">Read More</a></p>
              </div>
            </div>
          </div> -->
          
    
        </div>
      </div>



      <?php require_once '../layout/footer.php'; ?>

  </div>

  <script src="<?= asset('asset/js/jquery-3.3.1.min.js') ?>"></script>
  <script src="<?= asset('asset/js/jquery-migrate-3.0.1.min.js') ?>"></script>
  <script src="<?= asset('asset/js/jquery-ui.js') ?>"></script>
  <script src="<?= asset('asset/js/popper.min.js') ?>"></script>
  <script src="<?= asset('asset/js/bootstrap.min.js') ?>"></script>
  <script src="<?= asset('asset/js/owl.carousel.min.js') ?>"></script>
  <script src="<?= asset('asset/js/jquery.stellar.min.js') ?>"></script>
  <script src="<?= asset('asset/js/jquery.countdown.min.js') ?>"></script>
  <script src="<?= asset('asset/js/jquery.magnific-popup.min.js') ?>"></script>
  <script src="<?= asset('asset/js/bootstrap-datepicker.min.js') ?>"></script>
  <script src="<?= asset('asset/js/aos.js') ?>"></script>

  <script src="<?= asset('asset/js/main.js') ?>"></script>



</body>

</html>
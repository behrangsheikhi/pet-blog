<?php
require_once '../../function/helper.php';
require_once '../../function/connection.php';
require_once '../../function/check-login.php';

global $connect;

if (
    isset($_POST['title']) and $_POST['title'] !== ''
    and isset($_POST['body']) and $_POST['body'] !== ''
    and isset($_POST['cat_id']) and $_POST['cat_id'] !== ''
    and isset($_FILES['image']) and $_FILES['image']['name'] !== '') {
    $query = 'SELECT * FROM pet_blog_db.categories_tbl WHERE id = ? ';
    $statement = $connect->prepare($query);
    $statement->execute([$_POST['cat_id']]);
    $category = $statement->fetch();

    if ($category === false) {
        redirect('admin/post');
    }
//    check upload process
    $allowedMimes = ['jpg', 'jpeg', 'png', 'gif'];
    $imageMime = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
    if (!in_array($imageMime, $allowedMimes)) {
        redirect('admin/post');
    }
    $basePath = dirname(__DIR__, 2);
    $image = '/admin/assets/img/post/' . date("Y-m-d-H-i-s") . '.' . $imageMime;
    $imageUpload = move_uploaded_file($_FILES['image']['tmp_name'], $basePath . $image);

    if ($category !== false and $imageUpload !== false) {
        $query = 'INSERT INTO pet_blog_db.posts_tbl SET title = ?, body = ?, category_id  =?, image = ?, created_at = now()';
        $statement = $connect->prepare($query);
        $statement->execute([$_POST['title'], $_POST['body'], $_POST['cat_id'], $image]);
    }
    redirect('admin/post');

}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ایجاد پست</title>

    <!--    link css files here-->
    <link rel="stylesheet" href="<?= asset('admin/assets/css/bootstrap/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/grid.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/style.css') ?>">
    <!--    end linking css files-->
</head>

<body dir="rtl">

<?php require_once '../layout/header.php'; ?>
<section class="body-container">
    <?php require_once '../layout/sidebar.php'; ?>
    <section class="main-body" id="main-body">
        <div class="row">
            <span class="page-title">
                <h3 class="font-weight-bold m-3 pt-5">ایجاد پست جدید</h3>
            </span>
        </div>
        <hr>
        <div class="row">
            <form action="<?= url('admin/post/create.php') ?>" method="post" class="form-box"
                  enctype="multipart/form-data">
                <div class="row d-flex flex-column">
                    <label for="title" class="lbl_title">عنوان</label>
                    <input type="text" name="title" id="title" placeholder="عنوان پست را وارد کنید . . .">
                </div>
                <div class="row d-flex flex-column">
                    <label for="image" class="lbl_title">تصویر</label>
                    <input type="file" name="image" id="image">
                </div>
                <div class="row d-flex flex-column">
                    <label for="body" class="lbl_title">متن پست</label>
                    <textarea name="body" id="body" cols="30" rows="10" placeholder="متن پست را وارد کنید"></textarea>
                </div>
                <div class="row d-flex flex-column">
                    <label for="cat_id" class="lbl_title">دسته بندی</label>
                    <select name="cat_id" id="cat_id">
                        <option value="">دسته بندی را وارد کنید</option>
                        <?php
                        global $connect;
                        $query = 'SELECT * FROM pet_blog_db.categories_tbl WHERE status = 1';
                        $statement = $connect->prepare($query);
                        $statement->execute();
                        $categories = $statement->fetchAll();

                        foreach ($categories as $category) {
                            ?>
                            <option value="<?= $category->id; ?>"><?= $category->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="row">
                    <div class="btn_box">
                        <button type="submit" class="btn btn-primary" name="submit">ایجاد</button>
                        <button type="submit" class="btn btn-danger" href="<?= url('admin/category/index.php') ?>">
                            انصراف
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</section>


<script src="<?= asset('admin/assets/js/jquery.minv3.6.js') ?>"></script>
<script src="<?= asset('admin/assets/js/popper.js') ?>"></script>
<script src="<?= asset('admin/assets/js/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?= asset('admin/assets/js/grid.js') ?>"></script>

</body>

</html>

<?php
require_once '../../function/check-login.php';
require_once '../../function/helper.php';
require_once '../../function/connection.php';

global $connect;

if(isset($_GET['post_id']) and $_GET['post_id'] !== '') {
    $query = 'SELECT * FROM pet_blog_db.posts_tbl WHERE id = ?';
    $statement = $connect->prepare($query);
    $statement->execute([$_GET['post_id']]);
    $post = $statement->fetch();

    if ($post === false) {
        redirect('admin/post');
    } else {
        $status = ($post->status == 1) ? 0 : 1;
        $query = 'UPDATE pet_blog_db.posts_tbl SET status = ? WHERE id = ? ';
        $statement = $connect->prepare($query);
        $statement->execute([$status, $_GET['post_id']]);
    }
}
redirect('admin/post');

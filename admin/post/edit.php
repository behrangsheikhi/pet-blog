<?php
require_once '../../function/helper.php';
require_once '../../function/connection.php';
require_once '../../function/check-login.php';

global $connect;

if (!isset($_GET['post_id'])) {
    redirect('admin/post');
}

// check for existence of post_id
$query = 'SELECT * FROM pet_blog_db.posts_tbl WHERE id = ? ';
$statement = $connect->prepare($query);
$statement->execute([$_GET['post_id']]);
$post = $statement->fetch();

if ($post === false) {
    redirect('admin/post');
}

//update post if the post_id exists
if (
    isset($_POST['title']) and $_POST['title'] !== ''
    and isset($_POST['cat_id']) and $_POST['cat_id'] !== ''
    and isset($_POST['body']) and $_POST['body'] !== ''
) {
//    check the category exists or not
    $query = 'SELECT * FROM pet_blog_db.categories_tbl WHERE id = ? ';
    $statement = $connect->prepare($query);
    $statement->execute([$_POST['cat_id']]);
    $category = $statement->fetch();

    if (isset($_FILES['image']) and $_FILES['image']['name'] !== '') {
//        upload photo
        $allowedMimes = ['jpg', 'jpeg', 'png', 'gif'];
        $imageMime = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

        if (!in_array($imageMime, $allowedMimes)) {
            redirect('admin/post');
        }
        $basePath = dirname(__DIR__, 2);
        if (file_exists($basePath . $post->image)) {
            unlink($basePath . $post->image);
        }
        $image = '/admin/assets/img/post/' . date("Y-m-d-H-i-s") . '.' . $imageMime;
        $imageUpload = move_uploaded_file($_FILES['image']['tmp_name'], $basePath . $image);

        if ($category !== false and $imageUpload !== false) {
            $query = 'UPDATE pet_blog_db.posts_tbl SET title = ?, category_id=?, body = ?,image = ?, updated_at= now() WHERE id = ?';
            $statement = $connect->prepare($query);
            $statement->execute([$_POST['title'], $_POST['cat_id'], $_POST['body'], $image, $_GET['post_id']]);
        }
    } else {
//            if user doesn't change the photo
        if ($category !== false) {
            $query = 'UPDATE pet_blog_db.posts_tbl SET title = ?, category_id = ?, body = ?, updated_at = now() WHERE id = ?';
            $statement = $connect->prepare($query);
            $statement->execute([$_POST['title'], $_POST['cat_id'], $_POST['body'], $_GET['post_id']]);
        }
    }
    redirect('admin/post');

}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ویرایش پست</title>

    <!--    link css files here-->
    <link rel="stylesheet" href="<?= asset('admin/assets/css/bootstrap/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/grid.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/style.css') ?>">
    <!--    end linking css files-->
</head>
<body dir="rtl">

<?php require_once '../layout/header.php'; ?>
<section class="body-container">
    <?php require_once '../layout/sidebar.php'; ?>
    <section class="main-body" id="main-body">
        <div class="row">
            <span class="page-title">
                <h3 class="font-weight-bold m-3 pt-5">ویرایش پست</h3>
            </span>
        </div>
        <hr>
        <div class="row">
            <form action="<?= url('admin/post/edit.php?post_id=' . $_GET['post_id']); ?>" method="post" class="form-box"
                  enctype="multipart/form-data">
                <div class="row d-flex flex-column">
                    <label for="title" class="lbl_title">عنوان</label>
                    <input type="text" name="title" id="title" value="<?= $post->title; ?>">
                </div>
                <div class="row d-flex flex-column">
                    <label for="image" class="lbl_title">تصویر</label>
                    <input type="file" name="image" id="image">
                    <img src="<?= asset($post->image); ?>" alt="img" class="post-img mt-1">
                </div>
                <div class="row d-flex flex-column">
                    <label for="body" class="lbl_title">متن پست</label>
                    <textarea name="body" id="body" cols="30" rows="10"><?= $post->body; ?></textarea>
                </div>
                <div class="row d-flex flex-column">
                    <label for="cat_id" class="lbl_title">دسته بندی</label>
                    <select name="cat_id" id="cat_id">
                        <option value="">دسته بندی را وارد کنید</option>
                        <?php
                        global $connect;
                        $query = 'SELECT * FROM pet_blog_db.categories_tbl WHERE status = 1';
                        $statement = $connect->prepare($query);
                        $statement->execute();
                        $categories = $statement->fetchAll();

                        foreach ($categories as $category) {
                            ?>
                            <option value="<?= $category->id; ?>" <?php if ($category->id == $post->category_id) echo 'selected'; ?>><?= $category->name ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="row">
                    <div class="btn_box">
                        <button type="submit" class="btn btn-primary" name="submit">ویرایش</button>
                        <button type="submit" class="btn btn-danger" href="<?= url('admin/category/index.php') ?>">
                            انصراف
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</section>


<script src="<?= asset('admin/assets/js/jquery.minv3.6.js') ?>"></script>
<script src="<?= asset('admin/assets/js/popper.js') ?>"></script>
<script src="<?= asset('admin/assets/js/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?= asset('admin/assets/js/grid.js') ?>"></script>

</body>
</html>

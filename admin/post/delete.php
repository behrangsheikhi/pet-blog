<?php
require_once '../../function/helper.php';
require_once '../../function/check-login.php';
require_once '../../function/connection.php';


if (!isset($_GET['post_id']) and $_GET['post_id'] == '') {
    redirect('admin/post');
} else {
    global $connect;

    $query = 'SELECT * FROM pet_blog_db.posts_tbl WHERE id = ? ';
    $statement = $connect->prepare($query);
    $statement->execute([$_GET['post_id']]);
    $post = $statement->fetch();

    if ($post === false) {
        redirect('admin/post');
    } else {
        $basePath = dirname(__DIR__, 2);
        if (file_exists($basePath . $post->image)) {
            unlink($basePath . $post->image);
        }
        $query = 'DELETE FROM pet_blog_db.posts_tbl WHERE id = ? ';
        $statement = $connect->prepare($query);
        $statement->execute([$_GET['post_id']]);
    }
}
redirect('admin/post');
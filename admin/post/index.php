<?php

require_once '../../function/helper.php';
require_once '../../function/connection.php';
require_once '../../function/check-login.php';


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>مدیریت پست ها</title>
    <!--    link css files here-->
    <link rel="stylesheet" href="<?= asset('admin/assets/css/bootstrap/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/grid.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/style.css') ?>">
    <!--    end linking css files-->
</head>
<body dir="rtl">

<?php require_once '../layout/header.php'; ?>

<section class="body-container">

    <?php require_once '../layout/sidebar.php'; ?>

    <section id="main-body" class="main-body">
        <div class="row">
                        <span class="page-title font-weight-bold">
                        <h3 class="font-weight-bold m-3 pt-5">مدیریت | مشاهده پست ها</h3>
                    </span>
            <hr>
            <span class="page-detail">
                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام
                    </p>
                </span>
            <hr>
            <a href="<?= url('admin/post/create.php') ?>" class="btn btn-sm btn-success float-left m-3">ساخت
                پست</a>

            <section class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                    <tr>
                        <th>عنوان</th>
                        <th>بدنه پست</th>
                        <th>دسته بندی</th>
                        <th>وضعیت</th>
                        <th>تصویر</th>
                        <th>تاریخ ساخت</th>
                        <th>تاریخ بروزرسانی</th>
                        <th>تنظیمات</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    global $connect;
                    $query = 'SELECT pet_blog_db.posts_tbl.*, pet_blog_db.categories_tbl.name AS category_name FROM pet_blog_db.posts_tbl LEFT JOIN pet_blog_db.categories_tbl ON pet_blog_db.posts_tbl.category_id = pet_blog_db.categories_tbl.id ';
                    $statement = $connect->prepare($query);
                    $statement->execute();
                    $posts = $statement->fetchAll();

                    foreach ($posts as $post) {
                        ?>
                        <tr>
                            <td><?= $post->title; ?></td>
                            <td><?= substr($post->body, 0, 50) . '...'; ?></td>
                            <td><?= $post->category_name; ?></td>
                            <td>
                                <?php
                                if ($post->status == 1) { ?>
                                    <span class="text-success font-weight-bold">فعال</span>
                                <?php } else { ?>
                                    <span class="text-danger font-weight-bold">غیر فعال</span>
                                <?php } ?>
                            </td>
                            <td><img src="<?= asset($post->image); ?>" alt="image" class="post-img"></td>
                            <td><?= $post->created_at; ?></td>
                            <td>
                                <?php
                                if ($post->updated_at == null) echo "-"; else echo $post->updated_at;
                                ?>
                            </td>
                            <td class="btn-box">
                                <a href="<?= url('admin/post/edit.php?post_id=' . $post->id); ?>"
                                   class="btn btn-info btn-sm"><i class="fas fa-edit"></i></a>
                                <a href="<?= url('admin/post/delete.php?post_id=' . $post->id); ?>"
                                   class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>
                                <a href="<?= url('admin/post/change-status.php?post_id=' . $post->id); ?>"
                                   class="btn btn-warning btn-sm"><i class="fas fa-reply"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </section>
        </div>
    </section>


    <script src="<?= asset('admin/assets/js/jquery.minv3.6.js') ?>"></script>
    <script src="<?= asset('admin/assets/js/popper.js') ?>"></script>
    <script src="<?= asset('admin/assets/js/bootstrap/bootstrap.min.js') ?>"></script>
    <script src="<?= asset('admin/assets/js/grid.js') ?>"></script>
</body>
</html>

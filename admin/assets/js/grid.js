$(document).ready(function () {
  function removeAllToggleClass() {
    // alert('hi');
    $("#sidebar-toggle-hide").removeClass("d-md-inline");
    $("#sidebar-toggle-hide").removeClass("d-none");
    $("#sidebar-toggle-show").removeClass("d-inline");
    $("#sidebar-toggle-show").removeClass("d-md-none");
  }

  $("#sidebar-toggle-hide").click(function () {
    $("#sidebar").fadeOut(400);
    $("#main-body").animate({ width: "100%" }, 400);
    setTimeout(function () {
      removeAllToggleClass();
      $("#sidebar-toggle-hide").addClass("d-none");
      $("#sidebar-toggle-show").removeClass("d-none");
    }, 400);
  });

  $("#sidebar-toggle-show").click(function () {
    $("#sidebar").fadeIn(400);
    removeAllToggleClass();
    $("#sidebar-toggle-hide").removeClass("d-none");
    $("#sidebar-toggle-show").addClass("d-none");
  });

  $("#menu-toggle").click(function () {
    $("#body-header").toggle(400);
  });

  $("#search-toggle").click(function () {
    $("#search-toggle").removeClass("d-md-inline");
    $("#search-area").addClass("d-md-inline");
    $("#search-input").animate({ width: "12rem" }, 400);
  });

  $("#search-area-hide").click(function () {
    $("#search-input").animate({ width: "0" }, 400);
    setTimeout(function () {
      $("#search-toggle").addClass("d-md-inline");
      $("#search-area").removeClass("d-md-inline");
    }, 400);
  });

  $("#header-notification-toggle").click(function () {
    $("#header-notification").fadeToggle();
  });

  $("#header-comment-toggle").click(function () {
    $("#header-comment").fadeToggle();
  });

  $("#header-profile-setting-toggle").click(function () {
    $("#header-profile-setting").fadeToggle();
  });

  $(".sidebar-group-link").click(function () {
    $(".sidebar-group-link").removeClass("sidebar-group-link-active");
    $(".sidebar-group-link")
      .children(".sidebar-dropdown-toggle")
      .children(".angle")
      .removeClass("fa-angle-down");
    $(".sidebar-group-link")
      .children(".sidebar-dropdown-toggle")
      .children(".angle")
      .addClass("fa-angle-left");

    $(this).addClass("sidebar-group-link-active");
    $(this)
      .children(".sidebar-dropdown-toggle")
      .children(".angle")
      .removeClass("fa-angle-left");
    $(this)
      .children(".sidebar-dropdown-toggle")
      .children(".angle")
      .addClass("fa-angle-down");
  });
});

  $("#full-screen").click(function () {
    toggleFullScreen();
  });

  function toggleFullScreen() {
    if (
      (document.fullScreenElement && document.fullScreenElement != null) ||
      (!document.mozFullScreen && !document.webkitIsFullScreen)
    ) {
      if (document.documentElement.requestFullscreen) {
        document.documentElement.requestFullscreen();
      } else if (document.documentElement.mozRequestFullscreen) {
        document.documentElement.mozRequestFullscreen();
      } else if (document.documentElement.webkitRequestFullscreen) {
        document.documentElement.webkitRequestFullscreen(
          Element.Allow_KEYBOARD_INPUT
        );
      }
      $("#screen-compress").removeClass("d-none");
      $("#screen-expand").addClass("d-none");
    } else {
      if (document.cancelFullScreen) {
        document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      }
      $('#screen-compress').addClass('d-none');
      $('#screen-expand').removeClass('d-none');

    }
  }

<?php
require_once '../../function/helper.php';
require_once '../../function/connection.php';


global $connect;
$error = '';

if (
    isset($_POST['email']) and $_POST['email'] !== ''
    and isset($_POST['username']) and $_POST['username'] !== ''
    and isset($_POST['password']) and $_POST['password'] !== ''
    and isset($_POST['confirm']) and $_POST['confirm'] !== ''
    and isset($_POST['first_name']) and $_POST['first_name'] !== ''
    and isset($_POST['last_name']) and $_POST['last_name'] !== ''

) {

    if ($_POST['password'] === $_POST['confirm']) {
        if (strlen($_POST['password']) >= 6) {
            $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $query = 'SELECT * FROM pet_blog_db.users_tbl WHERE email = ?';
            $statement = $connect->prepare($query);
            $statement->execute([$_POST['email']]);
            $email = $statement->fetch();

            if ($email === false) {
                $query = 'SELECT * FROM pet_blog_db.users_tbl WHERE username = ?';
                $statement = $connect->prepare($query);
                $statement->execute([$_POST['username']]);
                $username = $statement->fetch();

                if ($username === false) {
                    $query = 'INSERT INTO pet_blog_db.users_tbl SET email = ?, username = ?, password=?,first_name=?,last_name=?, created_at = now()';
                    $statement = $connect->prepare($query);
                    $statement->execute([$_POST['email'], $_POST['username'], $password, $_POST['first_name'], $_POST['last_name']]);
                    redirect('admin/auth/login.php');
                } else {
                    $error = 'نام کاربری قبلا انتخاب شده است';
                }
            } else {
                $error = 'ایمیل قبلا انتخاب شده است';
            }
        } else {
            $error = 'رمز عبور حداقل باید شش کاراکتر باشد';
        }
    } else {
        $error = 'رمز عبور و تایید رمز عبور مطابقت ندارند';
    }

}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ثبت نام </title>
    <!--    link css files here-->
    <link rel="stylesheet" href="<?= asset('admin/assets/css/bootstrap/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/grid.css') ?>">

    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/aos.css') ?>">
    <!--    end linking css files-->
</head>
<body dir="rtl">
<?php require_once '../../layout/header.php'; ?>
<section class="body-container">

    <section class="main-body" id="main-body" style="overflow-x: hidden">
        <section class="row">
            <div class="login-box col-12 col-lg-6 pl-0 pr-0">

                <h2 class="title  w-100 text-center">ثبت نام </h2>
                <section class="bg-light my-0 px-2 error pt-3 pb-3">
                    <small class="text-danger">
                        <b>
                            <?php if ($error !== '') echo '<i class="fa fa fa-exclamation-circle"></i>' . ' ' . $error ?>
                        </b>
                    </small>
                </section>
<!--                form for getting user data-->
                <form action="<?= url('admin/auth/register.php') ?>" method="post">
                    <section class="form-group">
                        <label for="email">ایمیل</label>
                        <input type="email" name="email" id="email" autofocus placeholder="ایمیل خود را وارد کنید">
                    </section>
                    <section class="form-group">
                        <label for="username">نام کاربری</label>
                        <input type="text" name="username" id="username" placeholder="نام کاربری خود را وارد کنید">
                    </section>
                    <section class="form-group">
                        <label for="first_name">نام</label>
                        <input type="text" name="first_name" id="first_name" placeholder="نام خود را وارد کنید">
                    </section>
                    <section class="form-group">
                        <label for="last_name">نام خانوادگی</label>
                        <input type="text" name="last_name" id="last_name" placeholder="نام خانوادگی خود را وارد کنید">
                    </section>
                    <section class="form-group">
                        <label for="password">رمز عبور</label>
                        <input type="password" name="password" id="password" placeholder="رمز عبور را وارد کنید">
                    </section>
                    <section class="form-group">
                        <label for="confirm">تایید رمز عبور</label>
                        <input type="password" name="confirm" id="confirm" placeholder="رمز عبور را دوباره وارد کنید">
                    </section>

                    <section class="form-group" style="text-align: right !important;">
                        <a href="<?= url('admin/auth/login.php') ?>">
                            عضو شده اید؟وارد شوید
                        </a>
                    </section>
                    <section class="form-group d-flex flex-row justify-content-between btn-box"
                             style="margin-top: 2rem">
                        <input type="submit" value="ثبت نام" class="btn btn-primary">
                        <input type="submit" value="انصراف" class="btn btn-danger" href="<?= url('app') ?>">
                    </section>
                </form>

            </div>
        </section>
    </section>
</section>


<script src="<?= asset('admin/assets/js/jquery.minv3.6.js') ?>"></script>
<script src="<?= asset('admin/assets/js/popper.js') ?>"></script>
<script src="<?= asset('admin/assets/js/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?= asset('admin/assets/js/grid.js') ?>"></script>
<script src="<?= asset('asset/js/aos.js') ?>"></script>
<script src="<?= asset('asset/js/main.js') ?>"></script>
</body>
</html>

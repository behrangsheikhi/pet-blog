<?php
session_start();

require_once '../../function/helper.php';

session_destroy();

redirect('admin/auth/login.php');
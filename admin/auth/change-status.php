<?php
require_once '../../function/helper.php';
require_once '../../function/connection.php';
require_once '../../function/check-login.php';


global $connect;

if(!isset($_GET['user_id']) and $_GET['user_id'] === '')
{
    redirect('admin/auth');
}

$query = 'SELECT * FROM pet_blog_db.users_tbl WHERE id = ?';
$statement = $connect->prepare($query);
$statement->execute([$_GET['user_id']]);
$user = $statement->fetch();

if($user !== false)
{
    $status = ($user->status === 1) ? 2 : 1;
    $query = 'UPDATE pet_blog_db.users_tbl SET status = ? WHERE id = ? ';
    $statement = $connect->prepare($query);
    $statement->execute([$status,$_GET['user_id']]);
}
redirect('admin/auth');
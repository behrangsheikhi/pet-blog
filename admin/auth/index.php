<?php
require_once '../../function/helper.php';
require_once '../../function/connection.php';
require_once '../../function/check-login.php';


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>پنل مدیریت</title>

    <!--    link css files here-->
    <link rel="stylesheet" href="<?= asset('admin/assets/css/bootstrap/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/grid.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/style.css') ?>">
    <!--    end linking css files-->
</head>
<body dir="rtl">

<?php require_once '../layout/header.php'; ?>
<section class="body-container">
    <?php require_once '../layout/sidebar.php'; ?>
    <section class="main-body" id="main-body">
        <div class="row">
            <section class="mt-5 mb-2 d-flex justify-content-between align-items-center flex-column">
                <h3 class="page-title">
                    <strong>کاربران سایت</strong>
                </h3>

            </section>
            <!--    table of users-->
            <section class="table table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="text-center">نام</th>
                        <th class="text-center">نام خانوادگی</th>
                        <th class="text-center">نام کاربری</th>
                        <th class="text-center">ایمیل</th>
                        <th class="text-center">نقش</th>
                        <th class="text-center">وضعیت</th>
                        <th class="text-center">تاریخ عضویت</th>
                        <th class="text-center">تنظیمات</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    global $connect;
                    $query = 'SELECT * FROM pet_blog_db.users_tbl';
                    $statement = $connect->prepare($query);
                    $statement->execute();
                    $users = $statement->fetchAll();

                    foreach ($users as $user) {
                        ?>
                        <tr>
                            <td><?= $user->first_name; ?></td>
                            <td><?= $user->last_name; ?></td>
                            <td><?= $user->username; ?></td>
                            <td><?= $user->email; ?></td>
                            <td>
                                <?php
                                if ($user->auth === 1)
                                    echo '<strong class="text-success">مدیر</strong>';
                                else
                                    echo '<strong>کاربر عادی</strong>';
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($user->status === 1)
                                    echo '<strong class="text-success">فعال</strong>';
                                else
                                    echo '<strong class="text-danger">غیر فعال</strong>';
                                ?>
                            </td>
                            <td><?= $user->created_at; ?></td>
                            <td>
                                <section class="btn-box">
                                    <a href="<?= url('admin/auth/delete.php?user_id=' . $user->id); ?>"
                                       class="btn btn-danger">حذف</a>
                                    <a href="<?= url('admin/auth/edit.php?user_id=' . $user->id); ?>"
                                       class="btn btn-primary">ویرایش</a>
                                    <a href="<?= url('admin/auth/change-status.php?user_id=' . $user->id); ?>"
                                       class="btn btn-warning">تغییر وضع</a>
                                </section>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </section>
        </div>
    </section>
</section>


<script src="<?= asset('admin/assets/js/jquery.minv3.6.js') ?>"></script>
<script src="<?= asset('admin/assets/js/popper.js') ?>"></script>
<script src="<?= asset('admin/assets/js/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?= asset('admin/assets/js/grid.js') ?>"></script>

</body>
</html>
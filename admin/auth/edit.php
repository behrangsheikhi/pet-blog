<?php
require_once '../../function/helper.php';
require_once '../../function/connection.php';
require_once '../../function/check-login.php';

global $connect;
$error = '';
if (!isset($_GET['user_id'])) {
    redirect('admin/auth');
}
//check user
$query = 'SELECT * FROM pet_blog_db.users_tbl WHERE id = ?';
$statement = $connect->prepare($query);
$statement->execute([$_GET['user_id']]);
$user = $statement->fetch();
if ($user === false) {
    redirect('admin/auth');
}
if (
    isset($_POST['first_name']) and $_POST['first_name'] !== ''
    and isset($_POST['last_name']) and $_POST['last_name'] !== ''
    and isset($_POST['username']) and $_POST['username'] !== ''
    and isset($_POST['email']) and $_POST['email'] !== ''
    and isset($_POST['auth']) and $_POST['auth'] !== ''
) {
    $query = 'UPDATE pet_blog_db.users_tbl SET first_name = ?, last_name = ?, username = ?, email = ?, auth = ? WHERE id = ?';
    $statement = $connect->prepare($query);
    $statement->execute([$_POST['first_name'], $_POST['last_name'], $_POST['username'], $_POST['email'], $_POST['auth'], $_GET['user_id']]);

    redirect('admin/auth');
} else {
    if(!empty($_POST)){
        $error = 'ویرایش اطلاعات ناموفق بود';
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ایجاد کاربر جدید</title>

    <!--    link css files here-->
    <link rel="stylesheet" href="<?= asset('admin/assets/css/bootstrap/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/grid.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/style.css') ?>">
    <!--    end linking css files-->
</head>

<body dir="rtl">

<?php require_once '../layout/header.php'; ?>
<section class="body-container">
    <?php require_once '../layout/sidebar.php'; ?>
    <section class="main-body" id="main-body">
        <div class="row">
            <span class="page-title">
                <h3 class="font-weight-bold m-3 pt-5">ایجاد کاربر جدید</h3>
            </span>
        </div>
        <hr>
        <div class="row">
            <section class="bg-light my-0 px-2 error pt-3 pb-3">
                <small class="text-danger">
                    <b>
                        <?php if ($error !== '') echo '<i class="fa fa fa-exclamation-circle"></i>' . ' ' . $error ?>
                    </b>
                </small>
            </section>
            <form action="<?= url('admin/auth/edit.php?user_id=' . $_GET['user_id']); ?>" method="post" class="form-box"
                  enctype="multipart/form-data">
                <div class="row">
                    <div class="row d-flex flex-column">
                        <label for="first_name" class="lbl_title">نام</label>
                        <input type="text" name="first_name" id="first_name" value="<?= $user->first_name; ?>">
                    </div>
                    <div class="row d-flex flex-column">
                        <label for="last_name" class="lbl_title">نام خانوادگی</label>
                        <input type="text" name="last_name" id="last_name"
                               value="<?= $user->last_name; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="row d-flex flex-column">
                        <label for="username" class="lbl_title">نام کاربری</label>
                        <input type="text" name="username" value="<?= $user->username; ?>">
                    </div>
                    <div class="row d-flex flex-column">
                        <label for="email" class="lbl_title">ایمیل</label>
                        <input type="email" name="email" id="email" value="<?= $user->email; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="row d-flex flex-column">
                        <label for="auth" class="lbl_title">سطح دسترسی</label>
                        <select name="auth" id="auth">
                            <option value=""> نوع کاربری</option>
                            <?php
                            $query = 'SELECT * FROM pet_blog_db.authority_tbl';
                            $statement = $connect->prepare($query);
                            $statement->execute();
                            $authorities = $statement->fetchAll();
                            foreach ($authorities as $authority) {
                                ?>
                                <option value="<?= $authority->id; ?>"
                                    <?php
                                    if ($user->auth === $authority->id)
                                        echo 'selected'
                                    ?>>
                                    <?= $authority->name; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>

                </div>
                <div class="row">
                    <div class="btn_box">
                        <button type="submit" class="btn btn-primary" name="submit">ایجاد</button>
                        <button type="submit" class="btn btn-danger" href="<?= url('admin/users') ?>">
                            انصراف
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</section>


<script src="<?= asset('admin/assets/js/jquery.minv3.6.js') ?>"></script>
<script src="<?= asset('admin/assets/js/popper.js') ?>"></script>
<script src="<?= asset('admin/assets/js/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?= asset('admin/assets/js/grid.js') ?>"></script>

</body>

</html>

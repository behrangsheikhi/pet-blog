<?php
require_once '../../function/helper.php';
require_once '../../function/connection.php';
require_once '../../function/check-login.php';

global $connect;

if (isset($_GET['user_id']) and $_GET['user_id'] !== '') {
    $query = 'SELECT * FROM pet_blog_db.users_tbl WHERE id = ?';
    $statement = $connect->prepare($query);
    $statement->execute([$_GET['user_id']]);
    $user = $statement->fetch();

    if ($user === false) {
        redirect('admin/auth');
    } else {
        $query = ' DELETE FROM pet_blog_db.users_tbl WHERE id=? ';
        $statement = $connect->prepare($query);
        $statement->execute([$_GET['user_id']]);
    }
}
redirect('admin/auth');
<?php

session_start();

global $connect;

require_once '../../function/helper.php';
require_once '../../function/connection.php';

$error = '';

if (isset($_SESSION['user'])) {
    unset($_SESSION['user']);
}

if (
    isset($_POST['email']) and $_POST['email'] !== ''
    and isset($_POST['password']) and $_POST['password'] !== ''
) {
    $query = 'SELECT * FROM pet_blog_db.users_tbl WHERE email = ?';
    $statement = $connect->prepare($query);
    $statement->execute([$_POST['email']]);
    $user = $statement->fetch();

    if ($user !== false and $user->status === 1) {
        if (password_verify($_POST['password'], $user->password)) {
            $_SESSION['user'] = $user->first_name;
            if($user->auth === 1)
                redirect('admin');
            else
                redirect('app');
        } else {
            $error = 'رمز عبور اشتباه است';
        }
    } else if($user === false){
        $error = 'کاربر مورد نظر یافت نشد';
    }else{
        $error = 'کاربر مورد نظر غیرفعال شده است،با مدیر سایت تماس بگیرید';
    }
}
else{
    if(!empty($_POST)){
        $error = 'همه فیلد ها اجباری می باشند';
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ورود </title>
    <!--    link css files here-->
    <link rel="stylesheet" href="<?= asset('admin/assets/css/bootstrap/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/grid.css') ?>">

    <link rel="stylesheet" href="<?= asset('asset/css/style.css') ?>">
    <link rel="stylesheet" href="<?= asset('asset/css/aos.css') ?>">
    <!--    end linking css files-->
</head>
<body dir="rtl">
<?php require_once '../../layout/header.php'; ?>
<section class="body-container">

    <section class="main-body" id="main-body" style="overflow-x: hidden">
        <section class="row">
            <div class="login-box col-12 col-lg-6 pl-0 pr-0">

                <h2 class="title  w-100 text-center">ورود </h2>
                <section class="bg-light my-0 px-2 error pt-3 pb-3">
                    <small class="text-danger">
                        <b>
                            <?php if ($error !== '') echo '<i class="fa fa fa-exclamation-circle"></i>' . ' ' . $error ?>
                        </b>
                    </small>
                </section>
                <form action="#" method="post">
                    <section class="form-group">
                        <label for="email">ایمیل</label>
                        <input type="email" name="email" id="email" autofocus placeholder="ایمیل خود را وارد کنید">
                    </section>
                    <section class="form-group">
                        <label for="password">رمز عبور</label>
                        <input type="password" name="password" id="password" placeholder="رمز عبور را وارد کنید">
                    </section>
                    <section class="form-group d-flex flex-row justify-content-between">
                        <span class="rememberMe text-right">مرا به خاطر بسپار</span>
                        <input type="checkbox" name="rememberMe" id="rememberMe">
                    </section>
                    <section class="form-group" style="text-align: right !important;">
                        <a href="<?= url('admin/auth/register.php') ?>">
                            عضو نشده اید؟ثبت نام کنید
                        </a>
                    </section>
                    <section class="form-group d-flex flex-row justify-content-between btn-box"
                             style="margin-top: 2rem">
                        <input type="submit" value="ورود" class="btn btn-primary">
                        <input type="submit" value="انصراف" class="btn btn-danger">
                    </section>
                </form>

            </div>
        </section>
    </section>
</section>


<script src="<?= asset('admin/assets/js/jquery.minv3.6.js') ?>"></script>
<script src="<?= asset('admin/assets/js/popper.js') ?>"></script>
<script src="<?= asset('admin/assets/js/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?= asset('admin/assets/js/grid.js') ?>"></script>
<script src="<?= asset('asset/js/aos.js') ?>"></script>
<script src="<?= asset('asset/js/main.js') ?>"></script>
</body>
</html>

<aside id="sidebar" class="sidebar">
    <section class="sidebar-container">
        <section class="sidebar-wrapper">
            <a href="#">
                <section class="admin-logged-in d-flex flex-column">
                    <section class="sidebar-group-link">
                        <section class="sidebar-dropdown-toggle d-flex flex-column">
                            <span class="admin-profile"><img src="<?= asset('admin/assets/img/avatar-2.jpg') ?>"
                                                             alt="admin" class="admin-img"></span>
                            <span class="pointer text-light mt-2">
                               <?php
                                    if(isset($_SESSION['user']))
                                    {
                                        $logged_user = $_SESSION['user'];
                                        echo' سلام '. $logged_user;
                                    }
                               ?>

                           </span>
                        </section>
                    </section>
                </section>
                <?php
                if (isset($_SESSION['user'])) {
                    ?>
                    <a href="<?= url('admin/auth/logout.php') ?>" class="sidebar-link">
                        <i class="fas fa-power-off"></i>
                        <span>خروج از حساب</span>
                    </a>
                    <?php
                }
                ?>

            </a>
            <hr>
            <a href="<?= url('app') ?>" class="sidebar-link">
                <i class="fas fa-home"></i>
                <span>مشاهده سایت</span>
            </a>

            <a href="<?= url('admin') ?>" class="sidebar-link">
                <i class="fas fa-coffee"></i>
                <span>داشبورد</span>
            </a>

            <section class="sidebar-part-title">بخش محتوی</section>
            <a href="<?= url('admin/category/') ?>" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>دسته بندی ها</span>
            </a>

            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-chart-bar icon"></i>
                    <span>پست ها</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href="<?= url('admin/post/') ?>">مقالات</a>
                </section>
            </section>

            <section class="sidebar-part-title">بخش کاربران</section>
            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-users icon"></i>
                    <span>کاربران</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href="<?= url('admin/auth') ?>">لیست کاربران</a>
                </section>
            </section>

            <section class="sidebar-part-title">بخش ادمین</section>
            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-user-ninja icon"></i>
                    <span>مدیریت</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href="<?= url('admin/admin') ?>">لیست ادمین سایت</a>
                </section>
            </section>

            <section class="sidebar-part-title">بخش کاربران</section>
            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-users icon"></i>
                    <span>کاربران سایت</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href="<?= url('admin/users') ?>">لیست کاربران سایت</a>
                </section>
            </section>

            <section class="sidebar-part-title">اعلانات</section>
            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-envelope icon"></i>
                    <span>پیام ها | اعلانات سایت</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href="<?= url('admin/message') ?>">پیام ها</a>
                </section>
            </section>

        </section>
    </section>
</aside>
<?php
require_once '../../function/check-login.php';
require_once '../../function/connection.php';
require_once '../../function/helper.php';

global $connect;

if(isset($_GET['cat_id']) and $_GET['cat_id'] !== '')
{
    $query = 'DELETE FROM pet_blog_db.categories_tbl WHERE id = ?';
    $statement = $connect->prepare($query);
    $statement->execute([$_GET['cat_id']]);

    redirect('admin/category');
}
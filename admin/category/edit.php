<?php
require_once '../../function/connection.php';
require_once '../../function/helper.php';
require_once '../../function/check-login.php';

global $connect;
if(!isset($_GET['cat_id']) and $_GET['cat_id'] === '')
{
    redirect('admin/category');
}
else
{
    $query = 'SELECT * FROM pet_blog_db.categories_tbl WHERE id = ? ';
    $statement=$connect->prepare($query);
    $statement->execute([$_GET['cat_id']]);
    $category = $statement->fetch();

    if($category === false)
    {
        redirect('admin/category');
    }
    else
    {
        if(isset($_POST['name']) and $_POST['name'] !== '')
        {
            $query = 'UPDATE pet_blog_db.categories_tbl SET name = ?, updated_at = now() WHERE id = ?';
            $statement = $connect->prepare($query);
            $statement->execute([$_POST['name'],$_GET['cat_id']]);
            redirect('admin/category');
        }
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ویرایش دسته بندی</title>
    <!--    link css files here-->
    <link rel="stylesheet" href="<?= asset('admin/assets/css/bootstrap/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/grid.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/style.css') ?>">
    <!--    end linking css files-->
</head>
<body>

<section id="id">
    <?php require_once '../layout/header.php'; ?>
    <section class="container-fluid">

        <section class="row">
            <section class="col-md-2 p-0">
                <?php require_once '../layout/sidebar.php'; ?>
            </section>
            <section class="col-md-10 pt-3 mt-5">
                <h3 class="page-title mt-4 font-weight-bold">
                    ویرایش دسته بندی
                </h3>
                <form class="cat_create_form" action="<?= url('admin/category/edit.php?cat_id='.$_GET['cat_id']) ?>" method="post">
                    <section class="form-group">
                        <label for="name" class="float-right d-block font-weight-bold">نام</label>
                        <input type="text" class="form-control d-inline" name="name" id="name" value="<?= $category->name ?>">
                    </section>
                    <section class="form-group btn-box">
                        <button type="submit" class="btn btn-primary float-right">ویرایش</button>
                        <button type="submit" class="btn btn-danger float-right" value="<?= url('admin/category') ?>">
                            انصراف
                        </button>
                    </section>

                </form>

            </section>
        </section>
    </section>
</section>

<script src="<?= asset('admin/assets/js/jquery.minv3.6.js') ?>"></script>
<script src="<?= asset('admin/assets/js/popper.js') ?>"></script>
<script src="<?= asset('admin/assets/js/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?= asset('admin/assets/js/grid.js') ?>"></script>
</body>
</html>

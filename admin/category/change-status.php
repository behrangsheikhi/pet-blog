<?php
require_once '../../function/helper.php';
require_once '../../function/connection.php';
require_once '../../function/check-login.php';

global $connect;

if (!isset($_GET['cat_id']) or $_GET['cat_id'] === '') {
    redirect('admin/category');
}
$query = 'SELECT * FROM pet_blog_db.categories_tbl WHERE id = ?';
$statement = $connect->prepare($query);
$statement->execute([$_GET['cat_id']]);
$category = $statement->fetch();

if ($category !== false) {
    $query = 'SELECT * FROM pet_blog_db.posts_tbl';
    $statement = $connect->prepare($query);
    $statement->execute();
    $posts = $statement->fetchAll();
    foreach ($posts as $post) {
        if ($post->status == $category->status and $post->category_id == $category->id) {
//            change post status
            $postStatus = ($post->status == 1) ? 0 : 1;
            $query = 'UPDATE pet_blog_db.posts_tbl SET status = ? WHERE id = ? ';
            $statement = $connect->prepare($query);
            $statement->execute([$postStatus, $post->id]);

//            change category status
            $status = ($category->status == 1) ? 0 : 1;
            $query = 'UPDATE pet_blog_db.categories_tbl SET status = ? WHERE id = ? ';
            $statement = $connect->prepare($query);
            $statement->execute([$status, $_GET['cat_id']]);
        }
    }

    $status = ($category->status == 1) ? 0 : 1;
    $query = 'UPDATE pet_blog_db.categories_tbl SET status = ? WHERE id = ? ';
    $statement = $connect->prepare($query);
    $statement->execute([$status, $_GET['cat_id']]);
}
redirect('admin/category');

<?php
require_once '../../function/helper.php';
require_once '../../function/connection.php';
require_once '../../function/check-login.php';

if (isset($_POST['name']) and $_POST['name'] !== '') {
    global $connect;
    $query = 'INSERT INTO pet_blog_db.categories_tbl SET name = ?, created_at = now()';
    $statement = $connect->prepare($query);
    $statement->execute([$_POST['name']]);

    redirect('admin/category');
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ایجاد دسته بندی</title>
    <!--    link css files here-->
    <link rel="stylesheet" href="<?= asset('admin/assets/css/bootstrap/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/grid.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/style.css') ?>">
    <!--    end link css files here-->
</head>
<body dir="rtl">

<?php require_once '../layout/header.php'; ?>
<section class="body-container">
    <?php require_once '../layout/sidebar.php'; ?>
    <section class="main-body" id="main-body">
        <div class="row">
            <span class="page-title">
                <h3 class="font-weight-bold m-3 pt-5">ایجاد دسته بندی جدید</h3>
            </span>
        </div>
        <hr>
        <div class="row">
            <form action="<?= url('admin/category/create.php') ?>" method="post" class="form-box">
                <div class="row d-flex flex-column">
                    <label for="name" class="lbl_title">نام دسته بندی</label>
                    <input type="text" name="name" id="name" placeholder="نام دسته بندی را وارد کنید . . .">
                </div>
                <div class="row">
                    <div class="btn_box">
                        <button type="submit" class="btn btn-primary">ایجاد</button>
                        <button type="submit" class="btn btn-danger" href="<?= url('admin/category/index.php') ?>">انصراف</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</section>


<script src="<?= asset('admin/assets/js/jquery.minv3.6.js') ?>"></script>
<script src="<?= asset('admin/assets/js/popper.js') ?>"></script>
<script src="<?= asset('admin/assets/js/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?= asset('admin/assets/js/grid.js') ?>"></script>

</body>
</html>

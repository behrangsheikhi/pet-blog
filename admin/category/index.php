<?php
require_once '../../function/helper.php';
require_once '../../function/connection.php';
require_once '../../function/check-login.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>دسته بندی ها</title>

    <!--    link css files here-->
    <link rel="stylesheet" href="<?= asset('admin/assets/css/bootstrap/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/grid.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/style.css') ?>">
    <!--    end linking css files-->
</head>
<body dir="rtl">

<?php require_once '../layout/header.php'; ?>
<section class="body-container">
    <?php require_once '../layout/sidebar.php'; ?>
    <section class="main-body" id="main-body">
        <div class="row">
                        <span class="page-title font-weight-bold">
                        <h3 class="font-weight-bold m-3 pt-5">مدیریت | مشاهده دسته بندی ها</h3>
                    </span>
            <hr>
            <span class="page-detail">
                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام
                    </p>
                </span>
            <hr>
            <a href="<?= url('admin/category/create.php') ?>" class="btn btn-sm btn-success float-left m-3">ساخت دسته
                بندی</a>

            <section class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                    <tr>
                        <th>ردیف</th>
                        <th>نام</th>
                        <th>تاریخ ساخت</th>
                        <th>تاریخ بروزرسانی</th>
                        <th>وضعیت</th>
                        <th>تنظیمات</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    global $connect;
                    $query = 'SELECT * FROM pet_blog_db.categories_tbl ';
                    $statement = $connect->prepare($query);
                    $statement->execute();
                    $categories = $statement->fetchAll();

                    foreach ($categories as $category) {
                        ?>

                        <tr>
                            <td><?= $category->id ?></td>
                            <td><?= $category->name ?></td>
                            <td><?= $category->created_at ?></td>
                            <td><?php if (empty($category->updated_at)) echo '-'; else echo $category->updated_at; ?></td>
                            <td>
                                <?php if ($category->status == 1) { ?>
                                    <span class="text-success font-weight-bold">فعال</span>
                                <?php } else { ?>
                                    <span class="text-danger font-weight-bold">غیر فعال</span>
                                <?php } ?>
                            </td>
                            <td class="btn-box">
                                <a href="<?= url('admin/category/edit.php?cat_id=' . $category->id); ?>"
                                   class="btn btn-info btn-sm">ویرایش</a>
                                <a href="<?= url('admin/category/delete.php?cat_id=' . $category->id); ?>"
                                   class="btn btn-danger btn-sm">حذف</a>
                                <a href="<?= url('admin/category/change-status.php?cat_id=' . $category->id); ?>"
                                   class="btn btn-warning btn-sm">تغییر وضعیت</a>
                            </td>
                        </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </section>
        </div>
    </section>
</section>


<script src="<?= asset('admin/assets/js/jquery.minv3.6.js') ?>"></script>
<script src="<?= asset('admin/assets/js/popper.js') ?>"></script>
<script src="<?= asset('admin/assets/js/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?= asset('admin/assets/js/grid.js') ?>"></script>

</body>
</html>
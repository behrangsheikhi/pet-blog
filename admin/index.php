<?php
require_once '../function/helper.php';
require_once '../function/connection.php';
require_once '../function/check-login.php';


if (!isset($_SESSION)) {
    session_start();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>پنل مدیریت</title>

    <!--    link css files here-->
    <link rel="stylesheet" href="<?= asset('admin/assets/css/bootstrap/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/grid.css') ?>">
    <link rel="stylesheet" href="<?= asset('admin/assets/css/style.css') ?>">
    <!--    end linking css files-->
</head>
<body dir="rtl">

<?php require_once 'layout/header.php'; ?>
<section class="body-container">
    <?php require_once 'layout/sidebar.php'; ?>
    <section class="main-body" id="main-body" style="overflow-x: hidden">
        <section class="row">
            <section class="col-lg-3 col-md-6 col-12">
                <a href="#" class="text-decoration-none d-block mb-4">
                    <section class="card bg-custom-orange text-white">
                        <section class="card-body border-bottom">
                            <section class="d-flex justify-content-between">
                                <section class="info-box-body">
                                    <h5>300000 تومان</h5>
                                    <p>سود خالص 65000 تومان</p>
                                </section>
                                <section class="info-box-icon">
                                    <i class="fas fa-chart-line"></i>
                                </section>
                            </section>
                        </section>
                        <section class="card-footer info-box-footer">
                            <i class="fas fa-clock mx-2"></i>
                            بروزرسانی شده در 1401/01/02 ساعت 12:30
                        </section>
                    </section>
                </a>
            </section>
            <section class="col-lg-3 col-md-6 col-12">
                <a href="#" class="text-decoration-none d-block mb-4">
                    <section class="card bg-custom-green text-white">
                        <section class="card-body border-bottom">
                            <section class="d-flex justify-content-between">
                                <section class="info-box-body">
                                    <h5>300000 تومان</h5>
                                    <p>سود خالص 65000 تومان</p>
                                </section>
                                <section class="info-box-icon">
                                    <i class="fas fa-chart-line"></i>
                                </section>
                            </section>
                        </section>
                        <section class="card-footer info-box-footer">
                            <i class="fas fa-clock mx-2"></i>
                            بروزرسانی شده در 1401/01/02 ساعت 12:30
                        </section>
                    </section>
                </a>
            </section>
            <section class="col-lg-3 col-md-6 col-12">
                <a href="#" class="text-decoration-none d-block mb-4">
                    <section class="card bg-custom-pink text-white">
                        <section class="card-body border-bottom">
                            <section class="d-flex justify-content-between">
                                <section class="info-box-body">
                                    <h5>300000 تومان</h5>
                                    <p>سود خالص 65000 تومان</p>
                                </section>
                                <section class="info-box-icon">
                                    <i class="fas fa-chart-line"></i>
                                </section>
                            </section>
                        </section>
                        <section class="card-footer info-box-footer">
                            <i class="fas fa-clock mx-2"></i>
                            بروزرسانی شده در 1401/01/02 ساعت 12:30
                        </section>
                    </section>
                </a>
            </section>
            <section class="col-lg-3 col-md-6 col-12">
                <a href="#" class="text-decoration-none d-block mb-4">
                    <section class="card bg-custom-blue text-white">
                        <section class="card-body border-bottom">
                            <section class="d-flex justify-content-between">
                                <section class="info-box-body">
                                    <h5>300000 تومان</h5>
                                    <p>سود خالص 65000 تومان</p>
                                </section>
                                <section class="info-box-icon">
                                    <i class="fas fa-chart-line"></i>
                                </section>
                            </section>
                        </section>
                        <section class="card-footer info-box-footer">
                            <i class="fas fa-clock mx-2"></i>
                            بروزرسانی شده در 1401/01/02 ساعت 12:30
                        </section>
                    </section>
                </a>
            </section>
            <section class="col-lg-3 col-md-6 col-12">
                <a href="#" class="text-decoration-none d-block mb-4">
                    <section class="card bg-danger text-white">
                        <section class="card-body border-bottom">
                            <section class="d-flex justify-content-between">
                                <section class="info-box-body">
                                    <h5>300000 تومان</h5>
                                    <p>سود خالص 65000 تومان</p>
                                </section>
                                <section class="info-box-icon">
                                    <i class="fas fa-chart-line"></i>
                                </section>
                            </section>
                        </section>
                        <section class="card-footer info-box-footer">
                            <i class="fas fa-clock mx-2"></i>
                            بروزرسانی شده در 1401/01/02 ساعت 12:30
                        </section>
                    </section>
                </a>
            </section>
            <section class="col-lg-3 col-md-6 col-12">
                <a href="#" class="text-decoration-none d-block mb-4">
                    <section class="card bg-success text-white">
                        <section class="card-body border-bottom">
                            <section class="d-flex justify-content-between">
                                <section class="info-box-body">
                                    <h5>300000 تومان</h5>
                                    <p>سود خالص 65000 تومان</p>
                                </section>
                                <section class="info-box-icon">
                                    <i class="fas fa-chart-line"></i>
                                </section>
                            </section>
                        </section>
                        <section class="card-footer info-box-footer">
                            <i class="fas fa-clock mx-2"></i>
                            بروزرسانی شده در 1401/01/02 ساعت 12:30
                        </section>
                    </section>
                </a>
            </section>
            <section class="col-lg-3 col-md-6 col-12">
                <a href="#" class="text-decoration-none d-block mb-4">
                    <section class="card bg-warning text-white">
                        <section class="card-body border-bottom">
                            <section class="d-flex justify-content-between">
                                <section class="info-box-body">
                                    <h5>300000 تومان</h5>
                                    <p>سود خالص 65000 تومان</p>
                                </section>
                                <section class="info-box-icon">
                                    <i class="fas fa-chart-line"></i>
                                </section>
                            </section>
                        </section>
                        <section class="card-footer info-box-footer">
                            <i class="fas fa-clock mx-2"></i>
                            بروزرسانی شده در 1401/01/02 ساعت 12:30
                        </section>
                    </section>
                </a>
            </section>
            <section class="col-lg-3 col-md-6 col-12">
                <a href="#" class="text-decoration-none d-block mb-4">
                    <section class="card bg-primary text-white">
                        <section class="card-body border-bottom">
                            <section class="d-flex justify-content-between">
                                <section class="info-box-body">
                                    <h5>300000 تومان</h5>
                                    <p>سود خالص 65000 تومان</p>
                                </section>
                                <section class="info-box-icon">
                                    <i class="fas fa-chart-line"></i>
                                </section>
                            </section>
                        </section>
                        <section class="card-footer info-box-footer">
                            <i class="fas fa-clock mx-2"></i>
                            بروزرسانی شده در 1401/01/02 ساعت 12:30
                        </section>
                    </section>
                </a>
            </section>
        </section>

    </section>
</section>


<script src="<?= asset('admin/assets/js/jquery.minv3.6.js') ?>"></script>
<script src="<?= asset('admin/assets/js/popper.js') ?>"></script>
<script src="<?= asset('admin/assets/js/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?= asset('admin/assets/js/grid.js') ?>"></script>

</body>
</html>
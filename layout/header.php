
<header class="site-navbar" role="banner">
    <div class="container-fluid">
        <div class="row align-items-center">

            <div class="col-12 search-form-wrap js-search-form">
                <form method="get" action="#">
                    <input type="text" id="s" class="form-control" placeholder="Search...">
                    <button class="search-btn" type="submit"><span class="icon-search"></span></button>
                </form>
            </div>

            <div class="col-2 site-logo float-right">
                <a href="<?= url('app') ?>" class="text-black h2 mb-0">
                    <img src="<?= asset('asset/image/logo.png') ?>" alt="پت شاپ">
                </a>
            </div>

            <div class="col-10 text-right">
                <nav class="site-navigation" role="navigation">
                    <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block mb-0">
                        <?php
                        if (!isset($_SESSION['user'])) {
                            ?>
                            <li><a href="<?= url('admin/auth/login.php') ?>">ورود</a></li>
                            <li><a href="<?= url('admin/auth/register.php'); ?>">ثبت نام</a></li>

                        <?php } else { ?>
                            <li><a href="<?= url('admin/auth/login.php'); ?>">خروج</a></li>
                            <li>
                                <a href="<?= url('admin/auth/login.php'); ?>">
                                    <?php
                                    $logged_user = $_SESSION['user'];
                                    echo ' سلام ' . $logged_user;
                                    ?>
                                </a>
                            </li>
                        <?php } ?>
                        <?php
                        global $connect;
                        $query = 'SELECT * FROM pet_blog_db.categories_tbl';
                        $statement = $connect->prepare($query);
                        $statement->execute();
                        $categories = $statement->fetchAll();
                        foreach ($categories as $category) {
                            ?>
                            <li><a href="<?= url('app/category.php?cat_id=').$category->id; ?>"><?= $category->name; ?></a></li>
                        <?php } ?>

                        <li><a href="<?= url('app'); ?>">خانه</a></li>
                        
                    </ul>
                </nav>
                <a href="#" class="site-menu-toggle js-menu-toggle text-black d-inline-block d-lg-none"><span
                            class="icon-menu h3"></span></a></div>
        </div>

    </div>
</header>
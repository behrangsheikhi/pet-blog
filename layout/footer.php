

<div class="site-footer">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-4">
            <h3 class="footer-heading mb-4">درباره ما</h3>
            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای </p>
          </div>
          <div class="col-md-3 ml-auto">
            <!-- <h3 class="footer-heading mb-4">Navigation</h3> -->
            <ul class="list-unstyled float-left mr-5">
              <li><a href="#">درباره ما</a></li>
              <li><a href="#">تبلیغات</a></li>
              <li><a href="#">همکاری با ما</a></li>
              <li><a href="#">اشتراک</a></li>
            </ul>
            <ul class="list-unstyled float-left">
              <li><a href="#">سف</a></li>
              <li><a href="#">سبک زندگی</a></li>
              <li><a href="#">ورزش</a></li>
              <li><a href="#">طبیعت</a></li>
            </ul>
          </div>
          <div class="col-md-4">
            

            <div>
              <h3 class="footer-heading mb-4">با ما در ارتباط باشید</h3>
              <p>
                <a href="#"><span class="icon-facebook pt-2 pr-2 pb-2 pl-0"></span></a>
                <a href="#"><span class="icon-twitter p-2"></span></a>
                <a href="#"><span class="icon-instagram p-2"></span></a>
                <a href="#"><span class="icon-rss p-2"></span></a>
                <a href="#"><span class="icon-envelope p-2"></span></a>
              </p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12 text-center">
            <p>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              Copyright &copy; <script>document.write(new Date().getFullYear());</script> 
              تمامی حقوق محفوظ است
                  <i class="icon-coffee text-white"></i> و  <i class="icon-heart text-danger"></i>    ساخته شده با 
              </p>
          </div>
        </div>
      </div>
    </div>
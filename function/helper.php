<?php
use JetBrains\PhpStorm\NoReturn;

const BASE_URL = 'http://localhost/pet-blog/';

#[NoReturn] function redirect($url): void
{
    header('Location: ' . trim(BASE_URL, '/ ') . '/' . trim($url, '/ '));
    exit;
}

function asset($file): string
{
    return trim(BASE_URL, '/ ') . '/' . trim($file, '/ ');
}

function  url($url): string
{
    return trim(BASE_URL, '/ ') . '/' . trim($url, '/ ');
}

#[NoReturn] function dd($var): void
{
    echo '<pre>';
    var_dump($var);
    exit;
}



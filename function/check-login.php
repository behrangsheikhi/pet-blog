<?php
session_start();

if(!isset($_SESSION['user']))
{
    redirect('admin/auth/login.php');
}

<?php
    global $connect;

    $server = 'localhost';
    $username = 'root';
    $password = '';
    $dbname = 'pet_blog_db';

    try{
        $option = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ);
        $connect = new PDO("mysql:host=$server;dbname=$dbname",$username,$password,$option);

        return $connect;
    }
    catch(PDOException $exception){
        echo "Error : ".$exception->getMessage();
    }
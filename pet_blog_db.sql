-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2022 at 07:48 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pet_blog_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `authority_tbl`
--

CREATE TABLE `authority_tbl` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authority_tbl`
--

INSERT INTO `authority_tbl` (`id`, `name`) VALUES
(1, 'ادمین'),
(2, 'کاربر عادی');

-- --------------------------------------------------------

--
-- Table structure for table `categories_tbl`
--

CREATE TABLE `categories_tbl` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(5) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories_tbl`
--

INSERT INTO `categories_tbl` (`id`, `name`, `created_at`, `updated_at`, `status`) VALUES
(13, 'طراحی سایت', '2022-08-29 19:16:14', NULL, 0),
(14, 'برنامه نویسی موبایل', '2022-08-29 19:16:26', NULL, 1),
(15, 'سئو و مارکتینگ', '2022-08-29 19:16:36', NULL, 1),
(16, 'گرافیک،تدوین و طراحی لوگو', '2022-08-29 19:16:49', '2022-08-29 19:17:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `messages_tbl`
--

CREATE TABLE `messages_tbl` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts_tbl`
--

CREATE TABLE `posts_tbl` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` tinyint(5) NOT NULL DEFAULT 1,
  `image` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts_tbl`
--

INSERT INTO `posts_tbl` (`id`, `title`, `body`, `category_id`, `status`, `image`, `created_at`, `updated_at`) VALUES
(2, 'سئو و مارکتینگ و اثرات آن', 'طراحی سایت در ارومیه با اروم وب لذت بسیار شیرینی دارد چون کسب و کار شما میتواند با فروش بالا همراه باشد. شما میتوانید با اروم وب کسب و کار خود را به بهترین شکل ممکن رونق دهید. فقط جهت رونق کسب و کار باید برنامه ریزی بسیار درستی داشته باشید. امروز بیش از 90% کسب و کار ها به سمت فضای اینترنت قدم برمیدارند. اروم وب میتواند شما را در این راه موفق کند فقط کافیست راه اندازی کسب و کار خود را به اروم وب بسپارید. ما میتوانیم با کمترین قیمت و بهترین پشتیبانی خدمات طراحی سایت در ارومیه و بهینه سازی یا سئو را انجام دهیم چون موسسه ها و افراد بسیار زیادی با ما همکاری داشته اند و تمامی این موفقیت را به خاطر داشتن تیم حرفه ای هستیم. برای مشاوره رایگان با مشاوران ما به صورت کاملا رایگان با ما تماس بگیرید. مشاوران ما آماده راهنمایی شما به صورت کامل هستند.طراحی سایت در ارومیه با اروم وب لذت بسیار شیرینی دارد چون کسب و کار شما میتواند با فروش بالا همراه باشد. شما میتوانید با اروم وب کسب و کار خود را به بهترین شکل ممکن رونق دهید. فقط جهت رونق کسب و کار باید برنامه ریزی بسیار درستی داشته باشید. امروز بیش از 90% کسب و کار ها به سمت فضای اینترنت قدم برمیدارند. اروم وب میتواند شما را در این راه موفق کند فقط کافیست راه اندازی کسب و کار خود را به اروم وب بسپارید. ما میتوانیم با کمترین قیمت و بهترین پشتیبانی خدمات طراحی سایت در ارومیه و بهینه سازی یا سئو را انجام دهیم چون موسسه ها و افراد بسیار زیادی با ما همکاری داشته اند و تمامی این موفقیت را به خاطر داشتن تیم حرفه ای هستیم. برای مشاوره رایگان با مشاوران ما به صورت کاملا رایگان با ما تماس بگیرید. مشاوران ما آماده راهنمایی شما به صورت کامل هستند.', 15, 1, '/admin/assets/img/post/2022-08-29-22-16-40.jpg', '2022-08-30 00:46:40', NULL),
(3, 'گرافیک و طراحی لوگو', 'گرافیک و طراحی لوگو و بنر در ارومیه با اروم وب لذت بسیار شیرینی دارد چون کسب و کار شما میتواند با فروش بالا همراه باشد. شما میتوانید با اروم وب کسب و کار خود را به بهترین شکل ممکن رونق دهید. فقط جهت رونق کسب و کار باید برنامه ریزی بسیار درستی داشته باشید. امروز بیش از 90% کسب و کار ها به سمت فضای اینترنت قدم برمیدارند. اروم وب میتواند شما را در این راه موفق کند فقط کافیست راه اندازی کسب و کار خود را به اروم وب بسپارید. ما میتوانیم با کمترین قیمت و بهترین پشتیبانی خدمات طراحی سایت در ارومیه و بهینه سازی یا سئو را انجام دهیم چون موسسه ها و افراد بسیار زیادی با ما همکاری داشته اند و تمامی این موفقیت را به خاطر داشتن تیم حرفه ای هستیم. برای مشاوره رایگان با مشاوران ما به صورت کاملا رایگان با ما تماس بگیرید. مشاوران ما آماده راهنمایی شما به صورت کامل هستند.', 16, 1, '/admin/assets/img/post/2022-08-29-22-19-06.jpg', '2022-08-30 00:49:06', NULL),
(4, 'طراحی سایت در تهران', 'شرکت طراحی سایت بهپردازان با طراحی بیش از پانصد سایت اینترنتی در زمینه های طراحی سایتهای فروشگاهی، بازاریابی شبکه ای،  طراحی پورتال و غیره در سراسر جهان با بهره گیری از بروزترین تکنولوژی های طراحی سایت به همراه بهترین خدمات جانبی طراحی سایت نظیر بهینه سازی سایت، امنیت سایت، خدمات هاستینگ و طراحی سایت حرفه ای و ریسپانسیو با افتخار در کنار شماست.\r\nشرکت بهپردازان در زمینه طراحی گرافیکی سایت، اجرا، سئو، پشتیبانی، طراحی و پیاده سازی پایگاه های داده و با بیش از پانصد وب سایت طراحی شده فعالیت خود را ادامه داده و در این زمینه با اتکا به تیم خود بسیار سرآمد می باشد. طراحی سایت با جدیدترین تکنولوژی ها، سازگار با تمامی مرورگرها و نمایشگرها با ضمانت یکساله و ده سال خدمات پس از فروش می باشد.', 13, 0, '/admin/assets/img/post/2022-08-30-09-36-55.jpg', '2022-08-30 12:06:55', NULL),
(5, 'طراحی سایت در شیراز', 'شرکت بهپردازان در زمینه طراحی گرافیکی سایت، اجرا، سئو، پشتیبانی، طراحی و پیاده سازی پایگاه های داده و با بیش از پانصد وب سایت طراحی شده فعالیت خود را ادامه داده و در این زمینه با اتکا به تیم خود بسیار سرآمد می باشد. طراحی سایت با جدیدترین تکنولوژی ها، سازگار با تمامی مرورگرها و نمایشگرها با ضمانت یکساله و ده سال خدمات پس از فروش می باشد.شرکت بهپردازان در زمینه طراحی گرافیکی سایت، اجرا، سئو، پشتیبانی، طراحی و پیاده سازی پایگاه های داده و با بیش از پانصد وب سایت طراحی شده فعالیت خود را ادامه داده و در این زمینه با اتکا به تیم خود بسیار سرآمد می باشد. طراحی سایت با جدیدترین تکنولوژی ها، سازگار با تمامی مرورگرها و نمایشگرها با ضمانت یکساله و ده سال خدمات پس از فروش می باشد.شرکت بهپردازان در زمینه طراحی گرافیکی سایت، اجرا، سئو، پشتیبانی، طراحی و پیاده سازی پایگاه های داده و با بیش از پانصد وب سایت طراحی شده فعالیت خود را ادامه داده و در این زمینه با اتکا به تیم خود بسیار سرآمد می باشد. طراحی سایت با جدیدترین تکنولوژی ها، سازگار با تمامی مرورگرها و نمایشگرها با ضمانت یکساله و ده سال خدمات پس از فروش می باشد.', 13, 0, '/admin/assets/img/post/2022-08-30-09-39-24.jpg', '2022-08-30 12:09:24', NULL),
(6, 'رشد کسب و کار با طراحی وبسایت', 'شرکت بهپردازان در زمینه طراحی گرافیکی سایت، اجرا، سئو، پشتیبانی، طراحی و پیاده سازی پایگاه های داده و با بیش از پانصد وب سایت طراحی شده فعالیت خود را ادامه داده و در این زمینه با اتکا به تیم خود بسیار سرآمد می باشد. طراحی سایت با جدیدترین تکنولوژی ها، سازگار با تمامی مرورگرها و نمایشگرها با ضمانت یکساله و ده سال خدمات پس از فروش می باشد.\r\nشرکت بهپردازان در زمینه طراحی گرافیکی سایت، اجرا، سئو، پشتیبانی، طراحی و پیاده سازی پایگاه های داده و با بیش از پانصد وب سایت طراحی شده فعالیت خود را ادامه داده و در این زمینه با اتکا به تیم خود بسیار سرآمد می باشد. طراحی سایت با جدیدترین تکنولوژی ها، سازگار با تمامی مرورگرها و نمایشگرها با ضمانت یکساله و ده سال خدمات پس از فروش می باشد.\r\nشرکت بهپردازان در زمینه طراحی گرافیکی سایت، اجرا، سئو، پشتیبانی، طراحی و پیاده سازی پایگاه های داده و با بیش از پانصد وب سایت طراحی شده فعالیت خود را ادامه داده و در این زمینه با اتکا به تیم خود بسیار سرآمد می باشد. طراحی سایت با جدیدترین تکنولوژی ها، سازگار با تمامی مرورگرها و نمایشگرها با ضمانت یکساله و ده سال خدمات پس از فروش می باشد.', 13, 0, '/admin/assets/img/post/2022-08-30-09-41-40.jpg', '2022-08-30 12:11:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_tbl`
--

CREATE TABLE `users_tbl` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image` text DEFAULT '/admin/users/images/user.png',
  `password` text NOT NULL,
  `username` varchar(255) CHARACTER SET utf32 NOT NULL,
  `created_at` datetime NOT NULL,
  `status` tinyint(5) NOT NULL DEFAULT 1,
  `auth` tinyint(4) DEFAULT 2
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_tbl`
--

INSERT INTO `users_tbl` (`id`, `first_name`, `last_name`, `email`, `image`, `password`, `username`, `created_at`, `status`, `auth`) VALUES
(2, 'بهرنگ', NULL, 'behisheikhi990@gmail.com', '/admin/users/images/user.png', '$2y$10$REXz6ClJL6ALok.7VA/lme0aR8/GMdznNHkt5c.B/A6QLZBpa/viq', 'behrang', '2022-09-11 00:32:46', 1, 1),
(3, 'حسن', NULL, 'abdi@gmail.com', '/admin/users/images/user.png', '$2y$10$Btq6ajsg8CfAhpTYmQ3OEubvobjzfS/0bGZi5.AyjXa61rraJ3vr6', 'abdi', '2022-09-11 13:58:42', 1, 2),
(5, 'حسن', 'عبدی', 'hassan@gmail.com', '/admin/users/images/user.png', '$2y$10$uGz51DqAk6s8MWq9IdAHR.4Xs3Mk2WkmkODRr184TRZ2SbGzhiXDq', 'hassan', '2022-09-11 14:12:57', 2, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authority_tbl`
--
ALTER TABLE `authority_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `namd` (`name`);

--
-- Indexes for table `categories_tbl`
--
ALTER TABLE `categories_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages_tbl`
--
ALTER TABLE `messages_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts_tbl`
--
ALTER TABLE `posts_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_tbl`
--
ALTER TABLE `users_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authority_tbl`
--
ALTER TABLE `authority_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories_tbl`
--
ALTER TABLE `categories_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `messages_tbl`
--
ALTER TABLE `messages_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_tbl`
--
ALTER TABLE `posts_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users_tbl`
--
ALTER TABLE `users_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
